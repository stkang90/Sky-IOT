package com.sky.iot.posts.like.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sky.iot.posts.category.dao.Category;
import com.sky.iot.posts.comment.domain.Comment;
import com.sky.iot.posts.post.domain.Post;
import com.sky.iot.user.domain.User;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by stkang90 on 2017-02-13.
 */
@Entity(name = "post_like")
@IdClass(LikeId.class)
@Data
public class Like {

    @Id
    @ManyToOne
    @JoinColumn(name = "user_id", unique = true)
    @JsonIgnore
    private User user;

    @Id
    @ManyToOne
    @JoinColumn(name = "post_id", unique = true)
    @JsonIgnore
    private Post post;
}
