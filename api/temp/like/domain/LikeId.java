package com.sky.iot.posts.like.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sky.iot.posts.post.domain.Post;
import com.sky.iot.user.domain.User;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by stkang90 on 2017-02-25.
 */
public class LikeId implements Serializable {

    private User user;

    private Post post;

}
