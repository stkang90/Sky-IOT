package com.sky.iot.posts.comment.repository;

import com.sky.iot.posts.comment.domain.Comment;
import com.sky.iot.posts.post.domain.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    Page<Comment> findByPost_Id(long id, Pageable pageable);

}
