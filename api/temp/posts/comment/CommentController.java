package com.sky.iot.posts.comment;

import com.sky.iot.common.exception.BaseException;
import com.sky.iot.posts.comment.domain.Comment;
import com.sky.iot.posts.comment.repository.CommentRepository;
import com.sky.iot.posts.post.domain.Post;
import com.sky.iot.posts.post.repository.PostRepository;
import com.sky.iot.user.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by stkang90 on 2017-02-13.
 */
@RestController
@RequestMapping("/api/post/comment")
public class CommentController {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    @RequestMapping(value = "/{post_id}", method = RequestMethod.GET)
    public Page<Comment> getList(@PathVariable long post_id, @PageableDefault(direction = Sort.Direction.DESC) Pageable pageable) {
        return commentRepository.findByPost_Id(post_id, pageable);
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/{post_id}", method = RequestMethod.POST)
    public Comment insert(@PathVariable long post_id, Authentication authentication, String content) throws BaseException {
        Post post = postRepository.findOne(post_id);
        if (post == null) {
            throw new BaseException();
        }
        Comment comment = new Comment(post, (User) authentication.getPrincipal(), content);
        if ((comment = commentRepository.save(comment)) == null) {
            throw new BaseException();
        }
        post.setCommentCnt(post.getCommentCnt() + 1);
        postRepository.save(post);
        return comment;
    }
}
