package com.sky.iot.posts.comment.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sky.iot.posts.post.domain.Post;
import com.sky.iot.user.domain.User;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by stkang90 on 2017-02-13.
 */
@Entity
@Data
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(length = 500)
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @Temporal(TemporalType.TIMESTAMP)
    private Date udtDt = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    private Date crtDt;

    private boolean _delete = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id")
    @JsonIgnore
    private Post post;

    public Comment(){
    }

    public Comment(Post post, User user, String content) {
        this.post = post;
        this.user = user;
        this.content = content;
        this.crtDt = new Date();
    }

    public long getPostId(){
        return post.getId();
    }

    public long getUserId(){
        return user.getUserId();
    }

    public String getNickname(){
        return user.getNickname();
    }
}
