package com.sky.iot.posts.category.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sky.iot.posts.post.domain.Post;
import lombok.Data;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.Date;
import java.util.List;

/**
 * Created by stkang90 on 2017-02-13.
 */
@Entity
@Data
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false, length = 30, unique = true)
    private String name;

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Post> postList;

    private int postCnt;

    @Temporal(TemporalType.TIMESTAMP)
    private Date udtDt = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    private Date crtDt;

    private boolean _delete = false;

    public Category() {
        super();
    }

    public Category(String name) {
        this.name = name;
        this.crtDt = new Date();
    }
}
