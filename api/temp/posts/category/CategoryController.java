package com.sky.iot.posts.category;

import com.sky.iot.common.exception.BaseException;
import com.sky.iot.posts.category.dao.Category;
import com.sky.iot.posts.category.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by stkang90 on 2017-02-13.
 */
@RestController
@RequestMapping("/api/post/category")
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Category> getList() {
        return categoryRepository.findAll();
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Category get(@PathVariable long id) {
        return categoryRepository.findOne(id);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/{name}", method = RequestMethod.POST)
    public Category insert(@PathVariable String name) throws BaseException {
        Category category = categoryRepository.findByName(name);
        if (category != null) {
            throw new BaseException();
        }
        if ((category = categoryRepository.save(new Category(name))) == null) {
            throw new BaseException();
        }
        return category;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public Category update(@PathVariable long id, String name) throws BaseException {
        Category category = categoryRepository.findByName(name);
        if (category != null) {
            throw new BaseException();
        }
        if ((category = categoryRepository.findOne(id)) == null) {
            throw new BaseException();
        }
        category.setName(name);
        if ((category = categoryRepository.save(category)) == null) {
            throw new BaseException();
        }
        return category;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Category delete(@PathVariable long id) throws BaseException {
        Category category = categoryRepository.findOne(id);
        if (category == null) {
            throw new BaseException();
        }
        categoryRepository.delete(category);
        return category;
    }
}
