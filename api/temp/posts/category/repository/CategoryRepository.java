package com.sky.iot.posts.category.repository;

import com.sky.iot.posts.category.dao.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    Category findByName(String name);
}