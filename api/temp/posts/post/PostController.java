package com.sky.iot.posts.post;

import com.sky.iot.common.exception.BaseException;
import com.sky.iot.posts.category.dao.Category;
import com.sky.iot.posts.category.repository.CategoryRepository;
import com.sky.iot.posts.post.domain.Post;
import com.sky.iot.posts.post.repository.PostRepository;
import com.sky.iot.user.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by stkang90 on 2017-02-13.
 */
@RestController
@RequestMapping("/api/post")
public class PostController {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private PostRepository postRepository;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Post get(@PathVariable(value = "id") long id) throws BaseException {
        Post post = postRepository.findOne(id);
        if (post == null) {
            throw new BaseException();
        }
        post.setVisitCnt(post.getVisitCnt() + 1);
        if ((post = postRepository.save(post)) == null) {
            throw new BaseException();
        }
        return post;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Post delete(@PathVariable long id) throws BaseException {
        Post post = postRepository.findOne(id);
        if (post == null) {
            throw new BaseException();
        }
        postRepository.delete(post);
        return post;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/{category_id}", method = RequestMethod.POST)
    public Post insert(@PathVariable long category_id, Authentication authentication, String title, String content) throws BaseException {
        Category category = categoryRepository.findOne(category_id);
        if (category == null) {
            throw new BaseException();
        }
        category.setPostCnt(category.getPostCnt() + 1);
        Post post = postRepository.save(new Post(category, (User) authentication.getPrincipal(), title, content));
        if (post == null && categoryRepository.save(category) != null) {
            throw new BaseException();
        }
        return post;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public Post update(@PathVariable long id, long category_id, String title, String content) throws BaseException {
        Post post = postRepository.findOne(id);
        if (post == null) {
            throw new BaseException();
        }
        Category category = categoryRepository.findOne(category_id);
        if (category == null) {
            throw new BaseException();
        }
        post.setCategory(category);
        post.setTitle(title);
        post.setContentHtml(content);
        if ((post = postRepository.save(post)) == null) {
            throw new BaseException();
        }
        return post;
    }
}
