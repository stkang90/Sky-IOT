package com.sky.iot.posts.post.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sky.iot.posts.category.dao.Category;
import com.sky.iot.posts.comment.domain.Comment;
import com.sky.iot.user.domain.User;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by stkang90 on 2017-02-13.
 */
@Entity
@Data
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    @JoinColumn(name = "category_id")
    @JsonIgnore
    private Category category;

    @Column(nullable = false, length = 100)
    private String title;

    @Column(length = 200)
    private String content;

    private String html;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    private int visitCnt;

    private int likeCnt;

    @OneToMany(mappedBy = "post", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Comment> commentList;

    private int commentCnt;

    @Temporal(TemporalType.TIMESTAMP)
    private Date udtDt = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    private Date crtDt;

    private boolean _delete = false;

    public Post() {
    }

    public Post(Category category, User user, String title, String content) {
        this.category = category;
        this.user = user;
        this.title = title;
        this.setContentHtml(content);
        this.crtDt = new Date();
    }

    public long getCategoryId() {
        return category.getId();
    }

    public String getCategoryName() {
        return category.getName();
    }

    public long getUserId() {
        return user.getUserId();
    }

    public String getNickname() {
        return user.getNickname();
    }

    public void setContentHtml(String content) {
        this.content = content.replaceAll("</p>", "\n");
        this.content = this.content.replaceAll("<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>", "");
        this.content = this.content.substring(0, this.content.length() < 500 ? this.content.length() : 500);
        this.html = content;
    }
}
