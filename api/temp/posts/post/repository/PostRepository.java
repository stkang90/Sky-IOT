package com.sky.iot.posts.post.repository;

import com.sky.iot.posts.post.domain.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {

    Page<Post> findByCategory_Id(long id, Pageable pageable);

}
