package com.sky.iot.posts;

import com.sky.iot.common.exception.BaseException;
import com.sky.iot.posts.post.domain.Post;
import com.sky.iot.posts.post.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;

/**
 * Created by stkang90 on 2017-02-13.
 */
@RestController
@RequestMapping("/api/posts")
public class PostsController {

    @Autowired
    private PostRepository postRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<Post> getList(Long category_id, @PageableDefault(direction = Sort.Direction.DESC) Pageable pageable) {
        if (category_id != null) {
            return postRepository.findByCategory_Id(category_id, pageable);
        }
        return postRepository.findAll(pageable);
    }

    @RequestMapping(value = "/trending", method = RequestMethod.GET)
    public Page<Post> getTrendingList(@PageableDefault(direction = Sort.Direction.DESC, size = 5, sort = {"udtDt"}) Pageable pageable) {
        return postRepository.findAll(pageable);
    }
}
