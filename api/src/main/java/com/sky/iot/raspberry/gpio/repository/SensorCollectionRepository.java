package com.sky.iot.raspberry.gpio.repository;

import com.sky.iot.raspberry.gpio.domain.SensorStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SensorCollectionRepository extends JpaRepository<SensorStatus, Long> {


}
