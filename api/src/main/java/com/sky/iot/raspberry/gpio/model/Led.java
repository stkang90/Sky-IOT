package com.sky.iot.raspberry.gpio.model;

import lombok.Data;

@Data
public class Led {
    private int red;

    private int green;

    private int blue;
}