package com.sky.iot.raspberry.gpio.sensor;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by stkang90 on 2017-02-28.
 */
public class SensorCollector {
    private static final Logger logger = LoggerFactory.getLogger(SensorCollector.class);
    private final static int MIN_DELAY = 2000;
    private final static String PYTHON_COLLECT_FILE = "/home/pi/collect.py";

    private float mTemperature;
    private float mHumidity;
    private float mBrightness;
    private long mLastReadTime;
    private boolean mLastResult;

    public SensorCollector() {
        this.mLastReadTime = System.currentTimeMillis();
    }

    public synchronized boolean read(boolean force) {
        long readTime = System.currentTimeMillis();
        if (force || (System.currentTimeMillis() - mLastReadTime < MIN_DELAY)) {
            return mLastResult;
        }
        mLastReadTime = readTime;

        try {
            Runtime rt = Runtime.getRuntime();
            Process p = rt.exec(String.format("python %s", PYTHON_COLLECT_FILE));
            BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            if ((line = bri.readLine()) != null) {
                if (!line.contains("null")) {
                    JSONObject temp = new JSONObject(line);
                    mTemperature = (float) temp.optDouble("temperature", 0);
                    mHumidity = (float) temp.optDouble("humidity", 0);
                    mBrightness = (float) temp.optDouble("brightness", 0);
                    return mLastResult = true;
                }
            }
            bri.close();
            p.waitFor();
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return mLastResult = false;
    }

    public float getTemperature() {
        return mTemperature;
    }

    public float getHumidity() {
        return mHumidity;
    }

    public float getBrightness() {
        return mBrightness;
    }
}
