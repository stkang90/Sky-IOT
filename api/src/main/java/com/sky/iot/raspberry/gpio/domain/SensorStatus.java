package com.sky.iot.raspberry.gpio.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by stkang90 on 2017-03-01.
 */
@Entity
@Data
public class SensorStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private float temperature;

    private float humidity;

    private float brightness;

    private boolean heater;

    private boolean fan;

    @Temporal(TemporalType.TIMESTAMP)
    private Date crtDt;

    public void setSensor(float temperature, float humidity, float brightness) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.brightness = brightness;
    }

    public SensorStatus copy() {
        SensorStatus sensorStatus = new SensorStatus();
        sensorStatus.setSensor(temperature, humidity, brightness);
        sensorStatus.setHeater(heater);
        sensorStatus.setFan(fan);
        sensorStatus.setCrtDt(crtDt);
        return sensorStatus;
    }
}
