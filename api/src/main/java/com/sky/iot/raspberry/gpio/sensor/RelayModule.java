package com.sky.iot.raspberry.gpio.sensor;

/**
 * Created by stkang90 on 2017-03-05.
 */
public class RelayModule {

    private final static String PYTHON_RELAY_FILE = "/home/pi/relay.py";

    private final static int ONE_PIN = 23;
    private final static int TWO_PIN = 24;

    public static void runOne(boolean value) {
        runRelay(ONE_PIN, value ? 0 : 1);
    }

    public static void runTwo(boolean value) {
        runRelay(TWO_PIN, value ? 0 : 1);
    }

    private static void runRelay(int pin, int out) {
        try {
            Runtime rt = Runtime.getRuntime();
            Process p = rt.exec(String.format("python %s %d %d", PYTHON_RELAY_FILE, pin, out));
            p.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
