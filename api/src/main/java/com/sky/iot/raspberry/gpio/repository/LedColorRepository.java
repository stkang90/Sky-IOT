package com.sky.iot.raspberry.gpio.repository;

import com.sky.iot.raspberry.gpio.domain.LedColor;
import com.sky.iot.raspberry.gpio.domain.SensorStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LedColorRepository extends JpaRepository<LedColor, Long> {


}
