package com.sky.iot.raspberry.gpio;

import com.sky.iot.raspberry.gpio.domain.SensorStatus;
import com.sky.iot.raspberry.gpio.model.LastStatus;
import com.sky.iot.raspberry.gpio.repository.SensorCollectionRepository;
import com.sky.iot.raspberry.gpio.sensor.Led;
import com.sky.iot.raspberry.gpio.sensor.RelayModule;
import com.sky.iot.raspberry.gpio.sensor.SensorCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by stkang90 on 2017-02-28.
 */
@Service
@EnableScheduling
public class CollectScheduling {

    private static final Logger logger = LoggerFactory.getLogger(CollectScheduling.class);

    private static final float MIN_TEMPERATURE = 23.0f;

    private static final float MAX_TEMPERATURE = 26.0f;

    private static final float TEMPERATURE_MARGIN = 0.3f;

    private static final int DEFAULT_COLLECT = 15;

    private final static SensorCollector sensorCollector = new SensorCollector();

    @Autowired
    private SensorCollectionRepository statusCollectionRepository;

    private List<SensorStatus> sensorStatusList = new ArrayList<>();

    private static LastStatus lastStatus = new LastStatus();

    @PostConstruct
    private void scheduleConstruct() {
        RelayModule.runOne(false);
        RelayModule.runTwo(false);
        lastStatus.setLed(0, 0, 0);
    }

    @Scheduled(cron = "*/6 * * * * *")
    public void scheduleSensorStatus() {
        if (!sensorCollector.read(false)) {
            logger.debug("Collect Fail");
            return;
        }
        final SensorStatus sensorStatus;
        if (sensorStatusList.size() > DEFAULT_COLLECT) {
            sensorStatus = sensorStatusList.remove(0);
        } else {
            sensorStatus = new SensorStatus();
        }
        sensorStatus.setSensor(sensorCollector.getTemperature(), sensorCollector.getHumidity(), sensorCollector.getBrightness());
        sensorStatus.setCrtDt(new Date());
        sensorStatusList.add(sensorStatus);
        logger.debug(String.format("Temperature is : %.1f,  Humidity is : %.1f, Brightness : %.1f", sensorStatus.getTemperature(), sensorStatus.getHumidity(), sensorStatus.getBrightness()));
    }

    @Scheduled(cron = "*/30 * * * * *")
    public void scheduleStatus() {
        if (sensorStatusList.size() == 0) {
            return;
        }
        boolean statusChange = false;
        SensorStatus recentStatus = sensorStatusList.get(sensorStatusList.size() - 1);
        // 온도
        if (lastStatus.getTemperature() == null || recentStatus.getTemperature() != lastStatus.getTemperature()) {
            statusChange = true;
            lastStatus.setTemperature(recentStatus.getTemperature());
        }
        if (recentStatus.getTemperature() < MIN_TEMPERATURE || (lastStatus.isHeater() && recentStatus.getTemperature() < MIN_TEMPERATURE + TEMPERATURE_MARGIN)) {
            // 23도 이하일 때, 히터 가동 || 히터가 23.1도에 이미 가동중이면, 23.5도 까지 가동
            // 22 < 23, 23.1 < 23.5
            recentStatus.setHeater(true);
        } else if (recentStatus.getTemperature() > MAX_TEMPERATURE || (lastStatus.isFan() && recentStatus.getTemperature() > MAX_TEMPERATURE - TEMPERATURE_MARGIN)) {
            // 26도 이상일 때, 팬 가동, 팬이 25.9도에 이미 가동 중이면, 25.5도까지 가동
            // 27 > 26, 25.9 > 25.5
            recentStatus.setFan(true);
        } else {
            recentStatus.setHeater(false);
            recentStatus.setFan(false);
        }
        // 습도
        if (lastStatus.getHumidity() == null || recentStatus.getHumidity() != lastStatus.getHumidity()) {
            statusChange = true;
            lastStatus.setHumidity(recentStatus.getHumidity());
        }
        // 밝기
        if (lastStatus.getBrightness() == null || Math.abs(recentStatus.getBrightness() - lastStatus.getBrightness()) >= 5) {
            statusChange = true;
            lastStatus.setBrightness(recentStatus.getBrightness());
        }

        // 저장 및 동작
        if (statusChange) {
            if (recentStatus.isHeater() != lastStatus.isHeater()) {
                RelayModule.runOne(recentStatus.isHeater());
                lastStatus.setHeater(recentStatus.isHeater());
            } else if (recentStatus.isFan() != lastStatus.isFan()) {
                RelayModule.runTwo(recentStatus.isFan());
                lastStatus.setFan(recentStatus.isFan());
            }
            statusCollectionRepository.save(recentStatus.copy());
        }
    }

    @Scheduled(cron = "0 */5 * * * *")
    public void scheduleLedOff() {
        if (lastStatus.getLed().getRed() != 0 || lastStatus.getLed().getGreen() != 0 || lastStatus.getLed().getBlue() != 0) {
            Led.runColor(0, 0, 0);
            lastStatus.setLed(0, 0, 0);
        }
    }

    @Scheduled(cron = "0 0 23 * * *")
    public void scheduleDelete() {
        List<SensorStatus> collectionList = statusCollectionRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
        List<SensorStatus> deleteList = new ArrayList<>();
        final long prevWeek = System.currentTimeMillis() - 604800000;
        for(SensorStatus sensorStatus: collectionList){
            if(prevWeek > sensorStatus.getCrtDt().getTime()){
                deleteList.add(sensorStatus);
            }else{
                break;
            }
        }
        statusCollectionRepository.delete(deleteList);
        lastStatus = new LastStatus();
        lastStatus.setLed(0, 0, 0);
    }

    public List<SensorStatus> getRecentSensorStatusList() {
        return sensorStatusList;
    }

    public LastStatus getLastStatus() {
        return lastStatus;
    }

    public void setLastHeater(boolean heater){
        lastStatus.setHeater(heater);
    }
    public void setLastFan(boolean fan){
        lastStatus.setFan(fan);
    }

    public void setLastLedColor(int red, int green, int blue) {
        lastStatus.setLed(red, green, blue);
    }
}
