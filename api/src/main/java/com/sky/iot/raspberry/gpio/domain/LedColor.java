package com.sky.iot.raspberry.gpio.domain;

import com.sky.iot.user.domain.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by stkang90 on 2017-03-12.
 */
@Entity
@Data
public class LedColor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    private int red;

    private int green;

    private int blue;

    @Temporal(TemporalType.TIMESTAMP)
    private Date crtDt = new Date();

    public LedColor(User user, int red, int green, int blue) {
        this.user = user;
        this.red = red;
        this.green = green;
        this.blue = blue;
    }
}
