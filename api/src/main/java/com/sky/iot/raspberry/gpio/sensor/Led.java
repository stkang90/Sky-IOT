package com.sky.iot.raspberry.gpio.sensor;

/**
 * Created by stkang90 on 2017-03-12.
 */
public class Led {

    private final static String PYTHON_RELAY_FILE = "/home/pi/ledColor";

    public static void runColor(int red, int green, int blue) {
        try {
            Runtime rt = Runtime.getRuntime();
            Process p = rt.exec(String.format("%s %d %d %d", PYTHON_RELAY_FILE, red, green, blue));
            p.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
