package com.sky.iot.raspberry.gpio;

import com.sky.iot.raspberry.gpio.domain.LedColor;
import com.sky.iot.raspberry.gpio.domain.SensorStatus;
import com.sky.iot.raspberry.gpio.repository.LedColorRepository;
import com.sky.iot.raspberry.gpio.repository.SensorCollectionRepository;
import com.sky.iot.raspberry.gpio.sensor.Led;
import com.sky.iot.raspberry.gpio.sensor.RelayModule;
import com.sky.iot.user.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * Created by stkang90 on 2017-03-01.
 */
@RestController
@RequestMapping("/api/sensor")
public class SensorController {

    @Autowired
    private SensorCollectionRepository sensorCollectionRepository;

    @Autowired
    private LedColorRepository ledColorRepository;

    @Autowired
    private CollectScheduling collectScheduling;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public HashMap<String, Object> getRecentList(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 15) Pageable pageable) {
        HashMap<String, Object> senderData = new HashMap<>();
        senderData.put("recent", collectScheduling.getLastStatus());
        senderData.put("collect", sensorCollectionRepository.findAll(pageable));
        return senderData;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Page<SensorStatus> getList(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC, size = 15) Pageable pageable) {
        return sensorCollectionRepository.findAll(pageable);
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/led/{red}/{green}/{blue}", method = RequestMethod.GET)
    public LedColor setBrightness(Authentication authentication, @PathVariable int red, @PathVariable int green, @PathVariable int blue) {
        LedColor ledColor = new LedColor((User) authentication.getPrincipal(), red, green, blue);
        collectScheduling.setLastLedColor(red, green, blue);
        Led.runColor(red, green, blue);
        return ledColorRepository.save(ledColor);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/relay/{device}/{on}", method = RequestMethod.GET)
    public boolean setRelay(@PathVariable String device, @PathVariable boolean on) {
        if("heater".equals(device)){
            RelayModule.runOne(on);
            collectScheduling.setLastFan(on);
            return true;
        }else if("fan".equals(device)){
            RelayModule.runTwo(on);
            collectScheduling.setLastFan(on);
            return true;
        }
        return false;
    }
}
