package com.sky.iot.raspberry.gpio.model;

import lombok.Data;

import java.util.Date;

/**
 * Created by stkang90 on 2017-03-16.
 */
@Data
public class LastStatus {
    private Float temperature;
    private float temperatureHigh;
    private float temperatureLow;
    private Date temperatureHighDt;
    private Date temperatureLowDt;

    private Float humidity;
    private float humidityHigh;
    private float humidityLow;
    private Date humidityHighDt;
    private Date humidityLowDt;

    private Float brightness;
    private float brightnessHigh;
    private float brightnessLow;
    private Date brightnessHighDt;
    private Date brightnessLowDt;

    private boolean heater;
    private Date heaterDt;

    private boolean fan;
    private Date fanDt;

    private Led led = new Led();
    private Date ledDt;

    private Date updateDt = new Date();

    public void setTemperature(float temperature) {
        this.temperature = temperature;
        this.updateDt = new Date();
        if (temperatureHighDt == null || temperature >= temperatureHigh) {
            this.temperatureHigh = temperature;
            this.temperatureHighDt = updateDt;
        }
        if (temperatureLowDt == null || temperature <= temperatureLow) {
            this.temperatureLow = temperature;
            this.temperatureLowDt = updateDt;
        }
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
        this.updateDt = new Date();
        if (humidityHighDt == null || humidity >= humidityHigh) {
            this.humidityHigh = humidity;
            this.humidityHighDt = updateDt;
        }
        if (humidityLowDt == null || humidity <= humidityLow) {
            this.humidityLow = humidity;
            this.humidityLowDt = updateDt;
        }
    }

    public void setBrightness(float brightness) {
        this.brightness = brightness;
        this.updateDt = new Date();
        if (brightnessHighDt == null || brightness >= brightnessHigh) {
            this.brightnessHigh = brightness;
            this.brightnessHighDt = updateDt;
        }
        if (brightnessLowDt == null || brightness <= brightnessLow) {
            this.brightnessLow = brightness;
            this.brightnessLowDt = updateDt;
        }
    }

    public void setHeater(boolean heater) {
        this.updateDt = new Date();
        this.heater = heater;
        if (heaterDt == null || heater) {
            this.heaterDt = updateDt;
        }

    }

    public void setFan(boolean fan) {
        this.updateDt = new Date();
        this.fan = fan;
        if (fanDt == null || fan) {
            this.fanDt = this.updateDt;
        }
    }

    public void setLed(int red, int green, int blue) {
        this.led.setRed(red);
        this.led.setGreen(green);
        this.led.setBlue(blue);
        if (this.ledDt == null || (red != 0 || green != 0 || blue != 0)) {
            this.ledDt = new Date();
        }
    }

}
