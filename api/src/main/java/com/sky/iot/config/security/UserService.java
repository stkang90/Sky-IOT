package com.sky.iot.config.security;

import com.sky.iot.user.domain.User;
import com.sky.iot.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Created by stkang90 on 2017-02-17.
 */
@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (StringUtils.isEmpty(username)) {
            throw new UsernameNotFoundException("");
        }
        try {
            User user = userRepository.findOne(Long.valueOf(username));
            if (user != null) {
                return user;
            }
        }catch (Exception e){
        }
        User user = userRepository.findByNickname(username);
        if (user != null) {
            return user;
        }
        user = userRepository.findByEmail(username);
        if (user != null) {
            return user;
        }
        throw new UsernameNotFoundException(username);
    }

}
