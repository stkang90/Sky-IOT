package com.sky.iot.config.security.resource;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

/**
 * Created by stkang90 on 2017-02-16.
 */
@Configuration
@EnableResourceServer
public class OAuth2ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        /*http.headers().frameOptions().disable();
        http.authorizeRequests()
                .anyRequest().permitAll()
                .antMatchers("/api*//**").access("#oauth2.hasScope('read')");*/
        //http.authorizeRequests().antMatchers("/api/**").authenticated();
        http
                .headers().frameOptions().disable()
                .and()
                .authorizeRequests().
                anyRequest().permitAll();
    }

}
