package com.sky.iot.upload;

import com.sky.iot.upload.domain.UploadFile;
import com.sky.iot.upload.repository.UploadFileRepository;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * Created by stkang90 on 2017-03-17.
 */
@Component
public class Uploader {

    private static final String UPLOAD_PATH_BASE = "/usr/share/tomcat8/webapps/upload/";
    //private static final String UPLOAD_PATH_BASE = "C:/attachments/";

    @Autowired
    private UploadFileRepository uploadFileRepository;

    public UploadFile upload(MultipartFile sourceFile) throws IOException {
        String sourceFileName = sourceFile.getOriginalFilename();
        String sourceFileNameExtension = FilenameUtils.getExtension(sourceFileName).toLowerCase();
        File destinationFile;
        String destinationFileName;
        do {
            destinationFileName = RandomStringUtils.randomAlphanumeric(32) + "." + sourceFileNameExtension;
            destinationFile = new File(UPLOAD_PATH_BASE + destinationFileName);
        } while (destinationFile.exists());
        //destinationFile.getParentFile().mkdirs();
        sourceFile.transferTo(destinationFile);

        UploadFile uploadFile = new UploadFile(destinationFileName);
        uploadFile.setName(sourceFile.getOriginalFilename());
        uploadFile.setSize(sourceFile.getSize());
        uploadFile.setContentType(sourceFile.getContentType());

        uploadFileRepository.save(uploadFile);
        return uploadFile;
    }
}
