package com.sky.iot.upload.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by stkang90 on 2017-03-17.
 */
@Entity
@Data
public class UploadFile {
    @Id
    @Column
    private String fileName;

    @Column
    private String name;

    @Column
    private String contentType;

    @Column
    private long size;

    @Temporal(TemporalType.TIMESTAMP)
    private Date crtDt;

    public UploadFile() {
    }

    public UploadFile(String fileName){
        this.fileName = fileName;
    }
}
