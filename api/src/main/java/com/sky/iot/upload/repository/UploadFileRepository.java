package com.sky.iot.upload.repository;

import com.sky.iot.gallery.domain.GalleryCategory;
import com.sky.iot.upload.domain.UploadFile;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by stkang90 on 2017-03-17.
 */
public interface UploadFileRepository extends JpaRepository<UploadFile, Long> {
}
