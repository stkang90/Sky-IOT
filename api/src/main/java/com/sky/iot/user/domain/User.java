package com.sky.iot.user.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by stkang90 on 2017-02-13.
 */
@Entity
@Data
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long userId;

    @Column(nullable = false, length = 100, unique = true)
    @Length(min = 2, max = 100, message = "이메일은 2자이상 100자 이하로 입력하세요.")
    private String email;

    @Column(nullable = false, length = 30, unique = true)
    @Length(min = 2, max = 30, message = "닉네임은 2자이상 30자 이하로 입력하세요.")
    private String nickname;

    @Column(nullable = false, length = 256)
    @JsonIgnore
    private String password;

    @Temporal(TemporalType.TIMESTAMP)
    private Date udtDt = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    private Date crtDt;

    private boolean _delete = false;

    @Column(nullable = false, length = 50)
    private String authorities = "ROLE_USER";

    public User() {
    }

    public User(String nickname, String email, String password) {
        this.nickname = nickname;
        this.email = email;
        this.password = password;
        this.crtDt = new Date();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorityList = new ArrayList<>();
        String[] authorityArray = authorities.split(", ");
        for (String authority : authorityArray) {
            authorityList.add(new SimpleGrantedAuthority(authority));
        }
        return authorityList;
    }

    @Override
    public String getUsername() {
        return Long.toString(userId);
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }


    @Override
    public boolean isEnabled() {
        return true;
    }
}