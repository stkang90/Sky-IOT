package com.sky.iot.user;

import com.sky.iot.common.exception.BaseException;
import com.sky.iot.user.domain.User;
import com.sky.iot.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by stkang90 on 2017-02-13.
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ConsumerTokenServices tokenService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Map<String, Object> get(Authentication authentication) {
        Map<String, Object> result = new HashMap<>();
        if (authentication != null) {
            result.put("user", authentication.getPrincipal());
        }
        return result;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public User insert(String email, String nickname, String password) throws BaseException {
        if (userRepository.findByNickname(nickname) != null) {
            throw new BaseException();
        }
        if (userRepository.findByEmail(email) != null) {
            throw new BaseException();
        }
        User user = new User(nickname, email, passwordEncoder.encode(password));
        if ((user = userRepository.save(user)) == null) {
            throw new BaseException();
        }
        return user;
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/logout", method = RequestMethod.DELETE)
    public Map<String, Object> logout(String token) {
        tokenService.revokeToken(token);
        Map<String, Object> result = new HashMap<>();
        result.put("result", true);
        return result;
    }

    /*
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Page<User> getListPage(@PageableDefault(direction = Sort.Direction.DESC) Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/password", method = RequestMethod.POST)
    public User updatePassword(Authentication authentication, String old_password, String password) throws BaseException {
        User user = (User) authentication.getPrincipal();
        user = userRepository.findOne(user.getUserId());
        if (!user.getPassword().equals(passwordEncoder.encode(old_password))) {
            throw new BaseException();
        }
        user.setPassword(passwordEncoder.encode(password));
        if ((user = userRepository.save(user)) == null) {
            throw new BaseException();
        }
        return user;
    }


    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public User getName(@PathVariable String name) {
        return userRepository.findByNickname(name);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable long id) {
        userRepository.delete(id);
    }
*/

}
