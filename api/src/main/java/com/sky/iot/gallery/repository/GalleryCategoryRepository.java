package com.sky.iot.gallery.repository;

import com.sky.iot.gallery.domain.GalleryCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GalleryCategoryRepository extends JpaRepository<GalleryCategory, Long> {

}
