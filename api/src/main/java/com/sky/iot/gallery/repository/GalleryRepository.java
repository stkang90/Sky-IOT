package com.sky.iot.gallery.repository;

import com.sky.iot.gallery.domain.Gallery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GalleryRepository extends JpaRepository<Gallery, Long> {

    Page<Gallery> findByCategory_Id(long id, Pageable pageable);

}
