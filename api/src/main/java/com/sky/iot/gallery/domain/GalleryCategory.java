package com.sky.iot.gallery.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by stkang90 on 2017-02-13.
 */
@Entity
@Data
public class GalleryCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false, length = 30, unique = true)
    private String name;

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Gallery> gallery;

    @Temporal(TemporalType.TIMESTAMP)
    private Date crtDt;

}
