package com.sky.iot.gallery.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sky.iot.upload.domain.UploadFile;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by stkang90 on 2017-03-17.
 */
@Entity
@Data
public class Gallery {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    @JoinColumn(name = "category_id")
    @JsonIgnore
    private GalleryCategory category;

    @Column
    private String title;

    @Column
    private String description;

    @OneToOne
    @JoinColumn(name = "upload_file")
    private UploadFile image;

    @Temporal(TemporalType.TIMESTAMP)
    private Date crtDt;

    public String getImage(){
        return image.getFileName();
    }
}
