package com.sky.iot.gallery;

import com.sky.iot.common.exception.BaseException;
import com.sky.iot.gallery.domain.Gallery;
import com.sky.iot.gallery.domain.GalleryCategory;
import com.sky.iot.gallery.repository.GalleryCategoryRepository;
import com.sky.iot.gallery.repository.GalleryRepository;
import com.sky.iot.upload.Uploader;
import com.sky.iot.upload.domain.UploadFile;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * Created by stkang90 on 2017-03-17.
 */
@RestController
@RequestMapping("/api/gallery")
public class GalleryController {
    @Autowired
    private GalleryCategoryRepository galleryCategoryRepository;
    @Autowired
    private GalleryRepository galleryRepository;

    @Autowired
    private Uploader uploader;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<Gallery> getList(@PageableDefault(direction = Sort.Direction.DESC) Pageable pageable) {
        return galleryRepository.findAll(pageable);
    }

    @RequestMapping(value = "/{category_id}", method = RequestMethod.GET)
    public Page<Gallery> getCategoryList(@PathVariable long category_id, @PageableDefault(direction = Sort.Direction.DESC) Pageable pageable) {
        return galleryRepository.findByCategory_Id(category_id, pageable);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/upload/{category_id}", method = RequestMethod.POST)
    public boolean update(@RequestPart("img_file") MultipartFile imgFile, @PathVariable long category_id,
                          @RequestPart String title, @RequestPart String description) throws IOException, BaseException {
        if(!imgFile.getContentType().contains("image")){
            throw new BaseException();
        }

        GalleryCategory category = galleryCategoryRepository.findOne(category_id);
        if(category == null){
            throw new BaseException();
        }

        UploadFile uploadFile = uploader.upload(imgFile);
        if(uploadFile == null){
            throw new BaseException();
        }

        Gallery gallery = new Gallery();
        gallery.setCategory(category);
        gallery.setTitle(title);
        gallery.setDescription(description);
        gallery.setImage(uploadFile);
        galleryRepository.save(gallery);
        return true;
    }

}
