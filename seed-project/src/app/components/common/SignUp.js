import React from 'react'
import Api from '../api/api_client'

import {smallBox} from "../utils/actions/MessageActions";
import UiValidate from '../forms/validation/UiValidate'

export default class Upload extends React.Component {
  state = {
    email:'',
    nickname:'',
    password:''
  };
  onSubmit = (e) => {
    e.preventDefault();
    Api.post('user', this.state).then(({ data: { status, value} })=>{
      if (status === 200 && value) {
        $('#signUp').modal('hide');
        $('#signIn').modal('show');
        smallBox({
            content: "<p></p>회원가입에 성공하였습니다.",
            color: "#659265",
            iconSmall: "fa fa-check fa-2x fadeInRight animated",
            timeout: 4000
          });
      }
    });
  };
  onChangeEmail = (e) => {
    this.setState({email: e.target.value});
  }
  onChangeNickname = (e) => {
    this.setState({nickname: e.target.value});
  }
  onChangePassword = (e) => {
    this.setState({password: e.target.value});
  }
  render() {
    return (
        <div className="modal fade" id="signUp" tabIndex="-1" role="dialog" aria-labelledby="signUpLabel"
               aria-hidden="false">
            <div className="modal-dialog" style={ { width: '350px', margin: '10% auto' } }>
              <div className="modal-content">
                <UiValidate>
                  <form id="sign-up-form" className="smart-form client-form" onSubmit={this.onSubmit}>
                    <header className="modal-header">
                      <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                      &times;
                      </button>
                      <h4 className="modal-title" id="signUpLabel">Sign up</h4>
                    </header>
                    <fieldset >
                      <section>
                        <label className="label">이메일</label>
                        <label className="input"> <i className="icon-append fa fa-envelope-o"/>
                        <input type="email" name="email" data-smart-validate-input="" data-required="" data-email="" value={this.state.email} onChange={this.onChangeEmail}
                               data-message-required="이메일을 입력해주세요."
                               data-message-email="이메일 형식에 맞지 않습니다."/>
                        <b className="tooltip tooltip-top-right"><i className="fa fa-user txt-color-teal"/>
                               이메일을 입력해주세요.</b></label>
                      </section>
                      <section>
                        <label className="label">닉네임</label>
                        <label className="input"> <i className="icon-append fa fa-user"/>
                        <input type="text" name="nickname" data-smart-validate-input="" data-required="" value={this.state.nickname} onChange={this.onChangeNickname}
                               data-minlength="2" data-maxnlength="30"
                               data-message-required="별명을 입력해주세요."/>
                        <b className="tooltip tooltip-top-right"><i className="fa fa-user txt-color-teal"/>
                               별명을 입력해주세요.</b></label>
                      </section>
                      <section>
                        <label className="label">비밀번호</label>
                        <label className="input"> <i className="icon-append fa fa-lock"/>
                        <input type="password" name="password" data-smart-validate-input="" data-required="" value={this.state.password} onChange={this.onChangePassword}
                               data-minlength="3" data-maxnlength="20"
                               data-message="비밀번호를 입력해주세요."/>
                        <b className="tooltip tooltip-top-right"><i className="fa fa-lock txt-color-teal"/> 비밀번호를 입력해주세요.</b> </label>
                      </section>
                    </fieldset>
                    <footer className="modal-footer">
                      <button type="submit" className="btn btn-primary">
                        회원가입
                      </button>
                    </footer>
                  </form>
                </UiValidate>
              </div>
            </div>
          </div>
    )
  }
}
