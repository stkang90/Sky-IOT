import React from 'react'
import {connect} from 'react-redux';
import Api from '../api/api_client'

import {requestUserLogout} from "../user/UserActions";
import store from '../../store/configureStore'

import {smallBox} from "../utils/actions/MessageActions";
import FullScreen from './FullScreen'
import ToggleMenu from './ToggleMenu'

class Header extends React.Component {
  logout = () =>{
    Api.logout().then(({ data: { status, value} })=>{
      smallBox({
          content: "<p></p>로그아웃에 성공하였습니다.",
          color: "#659265",
          iconSmall: "fa fa-check fa-2x fadeInRight animated",
          timeout: 4000
        });
    });
  }
  render() {
    return <header id="header">
      <div id="logo-group">
                <span id="logo" className="font-lg text-center txt-color-white">
                  <b>Sky - IOT</b>
                </span>
        {/* Note: The activity badge color changes when clicked and resets the number to 0
         Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications */}

      </div>

      <div className="pull-right"  /*pulled right: nav area*/ >
        {(this.props.is_login?
          <div>
            {/* logout button */}
            <div id="logout" className="btn-header transparent pull-right">
                        <span> <a href="#" onClick={this.logout}><i
                          className="fa fa-sign-out"/></a> </span>
            </div>
            <FullScreen className="btn-header transparent pull-right"/>
          </div> :
          <div>
            <div className="btn-header transparent pull-right">
                        <span> <a href="#" data-toggle="modal" data-target="#signUp"><i
                          className="fa fa-plus-square-o"/></a> </span>
            </div>
            <div className="btn-header transparent pull-right">
                        <span> <a href="#" data-toggle="modal" data-target="#signIn"><i
                          className="fa fa-sign-in"/></a> </span>
            </div>
            <FullScreen className="btn-header transparent pull-right"/>
            <ToggleMenu className="btn-header transparent pull-right"/>
          </div>
        )}
      </div>
      {/* end pulled right: nav area */}
    </header>
  }
}

const mapStateToProps = (state)=>(state.user)

export default connect(mapStateToProps)(Header)
