import React from 'react'
import Api from '../api/api_client'

import {requestUserInfo} from "../user/UserActions";
import store from '../../store/configureStore'

import {smallBox} from "../utils/actions/MessageActions";
import UiValidate from '../forms/validation/UiValidate'

export default class Upload extends React.Component {
  state = {
    username:'',
    password:''
  };
  onSubmit = (e) => {
    e.preventDefault();
    Api.login(this.state).then(({ data: { status, value} })=>{
      if (status === 200 && value) {
        $('#signIn').modal('hide');
        store.dispatch(requestUserInfo());
        smallBox({
            content: "<p></p>로그인에 성공하였습니다.",
            color: "#659265",
            iconSmall: "fa fa-check fa-2x fadeInRight animated",
            timeout: 4000
          });
      }
    });
  };
  onChangeUsername = (e) => {
    this.setState({username: e.target.value});
  }
  onChangePassword = (e) => {
    this.setState({password: e.target.value});
  }
  render() {
    return (
      <div className="modal fade" id="signIn" tabIndex="-1" role="dialog" aria-labelledby="signInLabel"
             aria-hidden="false">
          <div className="modal-dialog" style={ { width: '350px', margin: '10% auto' } }>
            <div className="modal-content">
              <UiValidate>
                <form id="login-form" className="smart-form client-form" onSubmit={this.onSubmit}>
                  <header className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                    </button>
                    <h4 className="modal-title" id="signInLabel">Sign in</h4>
                  </header>
                  <fieldset >
                    <section>
                      <label className="label">이메일 또는 닉네임</label>
                      <label className="input"> <i className="icon-append fa fa-user"/>
                      <input type="text" name="username" data-smart-validate-input="" data-required="" value={this.state.username} onChange={this.onChangeUsername}
                             data-minlength="3" data-maxnlength="30"
                             data-message-required="이메일 또는 닉네임을 입력해주세요."/>
                      <b className="tooltip tooltip-top-right"><i className="fa fa-user txt-color-teal"/>
                             이메일 또는 닉네임을 입력해주세요.</b></label>
                    </section>
                    <section>
                      <label className="label">비밀번호</label>
                      <label className="input"> <i className="icon-append fa fa-lock"/>
                      <input type="password" name="password" data-smart-validate-input="" data-required="" value={this.state.password} onChange={this.onChangePassword}
                             data-minlength="3" data-maxnlength="20"
                             data-message="비밀번호를 입력해주세요."/>
                      <b className="tooltip tooltip-top-right"><i className="fa fa-lock txt-color-teal"/> 비밀번호를 입력해주세요.</b> </label>
                      <div className="note">
                        <a href="#/forgot">Forgot password?</a>
                      </div>
                    </section>
                  </fieldset>
                  <footer className="modal-footer">
                    <button type="submit" className="btn btn-primary">
                      로그인
                    </button>
                    <button className="btn btn-primary" data-toggle="modal" data-target="#signUp">
                        회원가입
                    </button>
                  </footer>
                </form>
              </UiValidate>
            </div>
          </div>
        </div>
    )
  }
}
