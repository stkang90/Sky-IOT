/**
 * Created by griga on 11/24/15.
 */

import React from 'react'

export default class Footer extends React.Component {
    render(){
        return (
            <div className="page-footer">
                <div className="row">
                    <div className="col-xs-12 col-sm-6">
                        <span className="txt-color-white">Copyright ⓒ Sky-IOT All right reserved. (stkang90@gmail.com)</span>
                    </div>
                </div>
            </div>
        )
    }
}
