/**
 * Created by griga on 11/17/16.
 */


import {USER_INFO, USER_LOGOUT} from './UserActions'

const userInitialState = {
    is_login: false,
    is_admin: false,
    user_info: {}
};

export default function userReducer (state = userInitialState, action ){
  switch (action.type){
    case USER_INFO:
      return Object.assign({}, state, {
                user_info: action.user,
                is_login: action.user != null,
                is_admin: action.user != null && action.user.authorities.length > 1
            });
    case USER_LOGOUT:
      return Object.assign({}, state, {
                user_info: {},
                is_login: false,
                is_admin: false
            });
    default:
      return state
  }
}
