/**
 * Created by griga on 11/24/15.
 */


import Api from '../api/api_client'

export const USER_INFO = 'USER_INFO'
export const USER_LOGOUT = 'USER_LOGOUT'

export function requestUserInfo()
{
  return (dispatch) => {
    return Api.get('user').then(({ data: { status, value} })=>{
      if (status === 200 && value) {
        dispatch({
          type: USER_INFO,
          user : value.user
        })
      }
    });
  }
}

export function requestUserLogout()
{
  return (dispatch) => {
    return dispatch({
      type: USER_LOGOUT
    });
  }
}
