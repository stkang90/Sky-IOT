import React from 'react'
import _ from 'lodash'

import 'script-loader!raphael'
import 'morris.js/morris.js'

Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";

    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;

    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
            default: return $1;
        }
    });
};
String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};

export default class MorrisGraph extends React.Component {
  componentDidMount() {
    this.renderChart(this.props.data)
  }

  componentWillReceiveProps(nextProps) {
    this.renderChart(nextProps.data)
  }

  renderChart(data) {
    if (data) {
      let options = {
        element: this.refs.morris,
        data: data,
        smooth: true,
        dateFormat:function (x) { return new Date(x).format("yyyy-MM-dd HH:mm:ss"); }
      };
      _.each(['xkey', 'ykeys', 'labels', 'pointSize', 'hideHover', 'stacked', 'grid',
        'barColors', 'formater', 'events', 'units', 'xLabels', 'xLabelFormat', 'parseTime', 'lineColors', 'resize'], function (key) {
        if (_.has(this.props, key)) options[key] = this.props[key];
      }.bind(this));

      switch (this.props.type) {
        case 'area':
          Morris.Area(options);
          break;
        case 'bar':
          Morris.Bar(options);
          break;
        case 'line':
          Morris.Line(options);
          break;
        case 'donut':
          Morris.Donut(options);
          break;
      }
    }
  }

  render() {
    return (
      <div className="chart no-padding" ref="morris"/>
    )
  }
}
