import React from 'react'


(function ($) {

  $.fn.SuperBox = function (options) {
    var superbox = $('<div class="superbox-show"></div>'),
      superboximg = $('<img src="" class="superbox-current-img"><div id="imgInfoBox" class="superbox-imageinfo inline-block"> <h1>Image Title</h1><span><p class="superbox-img-description">Image description</p><p class="superbox-crt-dt">2017/01/01</p></span> </div>'),
      superboxclose = $('<div class="superbox-close txt-color-white"><i class="fa fa-times fa-lg"></i></div>');

    superbox.append(superboximg).append(superboxclose);

    var imgInfoBox = $('.superbox-imageinfo');

    return this.each(function () {
      $('.superbox-list').click(function () {

        //$('.superbox-list', $(this)).click(function() {
        var $this = $(this);

        var currentimg = $this.find('.superbox-img'),
          imgData = currentimg.data('img'),
          imgDescription = currentimg.attr('alt') || "No description",
          imgTitle = currentimg.attr('title') || "No Title";

        //console.log(imgData, imgDescription, imgLink, imgTitle)

        superboximg.attr('src', imgData);

        $('.superbox-list').removeClass('active');
        $this.addClass('active');

        //$('#imgInfoBox em').text(imgLink);
        //$('#imgInfoBox >:first-child').text(imgTitle);
        //$('#imgInfoBox .superbox-img-description').text(imgDescription);

        superboximg.find('>:first-child').text(imgTitle);
        superboximg.find('.superbox-img-description').text(imgDescription);
        superboximg.find('.superbox-crt-dt').text(currentimg.attr('crt_dt'));

        //console.log("fierd")

        if ($('.superbox-current-img').css('opacity') == 0) {
          $('.superbox-current-img').animate({opacity: 1});
        }

        if ($(this).next().hasClass('superbox-show')) {
          if (superbox.is(":visible"))
            $('.superbox-list').removeClass('active');
          superbox.toggle();
        } else {
          superbox.insertAfter(this).css('display', 'block');
          $this.addClass('active');
        }

        $('html, body').animate({
          scrollTop: superbox.position().top - currentimg.width()
        }, 'medium');

      });

      $('.superbox').on('click', '.superbox-close', function () {
        $('.superbox-list').removeClass('active');
        $('.superbox-current-img').animate({opacity: 0}, 200, function () {
          $('.superbox-show').slideUp();
        });
      });

    });
  };
})(jQuery);

export default class SuperBoxGallery extends React.Component {
  componentDidUpdate(prevProps, prevState) {
    let element = $(this.refs.galleryContainer);
    element.SuperBox();
  }
  render() {
    let items = this.props.items || [];
    return (
      <div>
        <div ref="galleryContainer" className={this.props.className}>
          {items.map(function (item, idx) {
            return (
              <div className="superbox-list" key={idx}>
                <img src={item.image}
                     data-img={item.image}
                     alt={item.description}
                     title={item.title} className="superbox-img"/>
              </div>
            )
          })}

          <div className="superbox-float"/>

        </div>

      </div>
    )
  }
}
