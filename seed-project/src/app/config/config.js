
const SmartadminConfig = {
    menu_speed: 200,

    smartSkin: "smart-style-4",

    apiRootUrl: "assets/api",
    buildUrl: "build",
    skins: [
        {
            name: "smart-style-4",
            logo: "assets/img/logo-pale.png",
            class: "btn btn-xs btn-block txt-color-white margin-top-5",
            style: {
                background: '#bbc0cf',
                border: '1px solid #59779E',
                color: '#17273D !important'
            },
            label: "PixelSmash"
        }
    ]
};

SmartadminConfig.sound_path = "assets/sound/";
SmartadminConfig.sound_on = true;


/*
 * DEBUGGING MODE
 * debugState = true; will spit all debuging message inside browser console.
 * The colors are best displayed in chrome browser.
 */


SmartadminConfig.debugState = false;
SmartadminConfig.debugStyle = 'font-weight: bold; color: #00f;';
SmartadminConfig.debugStyle_green = 'font-weight: bold; font-style:italic; color: #46C246;';
SmartadminConfig.debugStyle_red = 'font-weight: bold; color: #ed1c24;';
SmartadminConfig.debugStyle_warning = 'background-color:yellow';
SmartadminConfig.debugStyle_success = 'background-color:green; font-weight:bold; color:#fff;';
SmartadminConfig.debugStyle_error = 'background-color:#ed1c24; font-weight:bold; color:#fff;';


export const config = SmartadminConfig;
