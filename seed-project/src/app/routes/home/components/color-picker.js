import React from 'react'
import reactCSS from 'reactcss'
import { SketchPicker } from 'react-color'

export default class ColorPicker extends React.Component {
  state = {
    displayColorPicker: false,
    color: {
      r: this.props.ledColor.red,
      g: this.props.ledColor.green,
      b: this.props.ledColor.green,
      a: '1',
    },
  };

  handleClick = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker })
  };

  handleClose = () => {
    this.setState({ displayColorPicker: false })
  };

  handleChange = (color) => {
    this.setState({ color: color.rgb })
    this.props.ledColor.red = color.rgb.r;
    this.props.ledColor.green = color.rgb.g;
    this.props.ledColor.blue = color.rgb.b;
  };
  render() {

    const styles = reactCSS({
      'default': {
        color: {
          width: '300px',
          height: '27px',
          borderRadius: '2px',
          background: `rgba(${ this.props.ledColor.red }, ${ this.props.ledColor.green }, ${ this.props.ledColor.blue }, ${ this.state.color.a })`,
        },
        swatch: {
          padding: '4px',
          background: '#fff',
          borderRadius: '1px',
          boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
          display: 'inline-block',
          cursor: 'pointer',
        },
        popover: {
          position: 'absolute',
          zIndex: '2',
        },
        cover: {
          position: 'fixed',
          top: '0px',
          right: '0px',
          bottom: '0px',
          left: '0px',
        },
      },
    });

    return (
      <div>
        <div style={ styles.swatch } onClick={ this.handleClick }>
          <div style={ styles.color } />
        </div>
        { this.state.displayColorPicker ? <div style={ styles.popover }>
          <div style={ styles.cover } onClick={ this.handleClose }/>
          <SketchPicker color={ this.state.color } onChange={ this.handleChange } />
        </div> : null }

      </div>
    )
  }
}
