/**
 * Created by griga on 11/30/15.
 <img src="http://192.168.100.102:8088/stream/video.mjpeg" width='100%'/>
 */

import React from 'react'
import {connect} from 'react-redux';

import WidgetGrid from '../../../components/widgets/WidgetGrid'
import JarvisWidget  from '../../../components/widgets/JarvisWidget'

import request from 'then-request'
import Api from '../../../components/api/api_client'

import {smallBox} from "../../../components/utils/actions/MessageActions";
import MorrisGraph from '../../../components/graphs/morris/MorrisGraph'
import IonSlider from '../../../components/forms/inputs/IonSlider'
import ColorPicker from '../components/color-picker';

class Home extends React.Component {
  state = {led:{
    red: 0,
    green: 0,
    blue: 0
  }};
  showLive = false;

  setLedBrightness = () => {
    var slider = $("#led_white").data("ionRangeSlider");
    const brightness = parseInt(255 * slider.result.from / 100);
    this.setLed(brightness, brightness, brightness);
  };
  setLedColor = () => {
    this.setLed(this.state.led.red, this.state.led.green, this.state.led.blue);
  };
  setLed = (red, green, blue) => {
    if(!this.props.is_login){
      $('#signIn').modal('show');
      smallBox({
          content: "<p></p>로그인이 필요합니다.",
          color: "#c26565",
          iconSmall: "fa fa-ban fa-2x fadeInRight animated",
          timeout: 4000
      });
      return;
    }
    SensorLed.setColor(red, green, blue).then(()=>{
      smallBox({
          content: "<p></p><i>조명이 설정되었습니다.<br />5분 뒤에 꺼집니다.</i>",
          color: "#659265",
          iconSmall: "fa fa-check fa-2x fadeInRight animated",
          timeout: 4000
        });
    });
  };
  setRelayHeater = () => {
    if(!this.props.is_login){
      $('#signIn').modal('show');
      smallBox({
          content: "<p></p>로그인이 필요합니다.",
          color: "#c26565",
          iconSmall: "fa fa-ban fa-2x fadeInRight animated",
          timeout: 4000
      });
      return;
    }
    if(!this.props.is_admin){
      smallBox({
          content: "<p></p>권한이 없습니다.",
          color: "#c26565",
          iconSmall: "fa fa-ban fa-2x fadeInRight animated",
          timeout: 4000
      });
      return;
    }
    Api.get('sensor/relay/heater/' + (this.state.heater? "false":"true")).then(({ data: { status, value} })=>{
      if (status === 200 && value) {
        this.setState({
          heater: this.state.heater ? false : true
        });
      }
    });
  };
  setRelayFan = () => {
    if(!this.props.is_login){
      $('#signIn').modal('show');
      smallBox({
          content: "<p></p>로그인이 필요합니다.",
          color: "#c26565",
          iconSmall: "fa fa-ban fa-2x fadeInRight animated",
          timeout: 4000
      });
      return;
    }
    if(!this.props.is_admin){
      smallBox({
          content: "<p></p>권한이 없습니다.",
          color: "#c26565",
          iconSmall: "fa fa-ban fa-2x fadeInRight animated",
          timeout: 4000
      });
      return;
    }
    Api.get('sensor/relay/fan/' + (this.state.fan? "false":"true")).then(({ data: { status, value} })=>{
      if (status === 200 && value) {
        this.setState({
          fan: this.state.fan ? false : true
        });
      }
    });
  };
  convertDateFormat = (data) => {
    if(data == null){
      return;
    }
    return new Date( data ).toISOString().substr(0,10);
  }
  tick = () => {
    SensorDataSource.getRecentDataList().then((data)=>{
      const recentSensor = SensorDataSource.getRecentData();
      this.setState({
        ...SensorDataSource.getRecentData(),
          sensorData: data
      });
      const ledWhite = (recentSensor.led.red == recentSensor.led.green == recentSensor.led.blue)? recentSensor.led.red : 0;
      var slider = $("#led_white").data("ionRangeSlider");
      slider.update({
          from: ledWhite
      });
    });
  }

  componentDidMount() {
    this.interval = setInterval(this.tick, 30000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
    clearInterval(this.liveCam);
  }

  componentWillMount() {
    this.tick();
    this.liveCam = setInterval(()=>{
      this.setState({
        showLive : true
      });
      clearInterval(this.liveCam);
    }, 5000);
  }
  render() {
    return (
      <div id="content">
        <div className="row">
          <h1 className="txt-color-blueDark text-center">
            하늘다람쥐 아롱이와 다롱이의 <b>IOT</b>
          </h1>
        </div>
        <WidgetGrid>
          <div className="row">
            <article className="col-xs-12 col-sm-7">
              <JarvisWidget colorbutton={false} togglebutton={false} deletebutton={false} editbutton={false}
                color="red">
                <header>
                    <span className="widget-icon"> <i className="fa fa-video-camera"/> </span>
                    <h2><strong>라이브</strong> 비디오</h2>
                </header>
                <div className="no-padding" >
                  {(this.state.showLive?
                    <div>
                    <img src="http://stkang.mooo.com:8088/stream/video.mjpeg" width='100%'/>
                    </div> :
                    <div>
                      <p className="alert alert-warning" style={ {marginBottom: '0px'} }>
                        <i className="fa fa-gear fa-spin"/> 라이브 비디오 로딩 중입니다...
                      </p>
                      <img src="https://ncache.ilbe.com/files/attach/new/20140808/377678/3415154174/4050210387/02a174684510be5fd28adfa1bdffa935.jpg" width='100%' height='425px'/>
                    </div>
                  )}

                </div>
              </JarvisWidget>
            </article>

            <article className="col-xs-12 col-sm-5">
              <JarvisWidget colorbutton={false} togglebutton={false} deletebutton={false} editbutton={false}
                fullscreenbutton={false} color="blue">
                <header>
                    <span className="widget-icon"> <i className="fa fa-smile-o"/> </span>
                    <h2><strong>현재 상태</strong> (최적 온도 : 23℃ ~ 26℃)</h2>
                </header>
                <div>
                  <div className="row">
                    <div className="col-xs-4">
                      <div className="well well-sm bg-color-redLight txt-color-white text-center">
                        <h6>온도</h6>
                        <h5>{this.state.temperature} ℃</h5>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div className="well well-sm bg-color-blue txt-color-white text-center">
                        <h6>습도</h6>
                        <h5>{this.state.humidity} %</h5>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div className="well well-sm bg-color-yellow txt-color-white text-center">
                        <h6>조도</h6>
                        <h5>{this.state.brightness} lux</h5>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-xs-6">
                      <a href="#" onClick={this.setRelayHeater}>
                        {(this.state.heater?
                          <div className="well well-sm bg-color-red txt-color-white text-center" >
                            <h6>온열기</h6>
                            <span className="badge bg-color-redLight">ON</span>
                          </div> :
                          <div className="well well-sm bg-color-blueLight txt-color-white text-center" >
                            <h6>온열기</h6>
                            <span className="badge  bg-color-blue">OFF</span>
                          </div>
                        )}
                      </a>
                    </div>
                    <div className="col-xs-6">
                      <a href="#" onClick={this.setRelayFan}>
                        {(this.state.fan?
                          <div className="well well-sm bg-color-blue txt-color-white text-center">
                            <h6>선풍기</h6>
                            <span className="badge bg-color-redLight">ON</span>
                          </div> :
                          <div className="well well-sm bg-color-blueLight txt-color-white text-center">
                            <h6>선풍기</h6>
                            <span className="badge bg-color-blue">OFF</span>
                          </div>
                        )}
                      </a>
                    </div>
                  </div>
                </div>
              </JarvisWidget>
            </article>
            <article className="col-xs-12 col-sm-5">
              <JarvisWidget colorbutton={false} togglebutton={false} deletebutton={false} editbutton={false}
                fullscreenbutton={false}
                color="blue">
                <header>
                  <span className="widget-icon"> <i className="fa fa-lightbulb-o"/> </span>
                  <h2><strong>조명 설정</strong></h2>
                  <ul className="nav nav-tabs pull-right in">
                    <li className="active">
                      <a data-toggle="tab" href="#white"> White </a>
                    </li>
                    <li>
                      <a data-toggle="tab" href="#color"> Color </a>
                    </li>
                  </ul>
                </header>
                <div className="widget-body">
                  <div className="tab-content">
                    <div className="tab-pane active" id="white">
                      <br /><center>
                        <IonSlider id="led_white" type="text" data-min="0"
                                         data-from="0" data-max="100" data-type="single"
                                         data-step="0" data-postfix=" %"
                                         data-prettify="false"
                                         data-hasgrid="true"/>
                      </center><br />
                      <div className="widget-footer">
                        <button className="btn btn-sm btn-primary" type="button" onClick={ this.setLedBrightness }>
                          설정
                        </button>
                      </div>
                    </div>
                    <div className="tab-pane" id="color">
                      <br /><center>
                        <ColorPicker id='led_color' ledColor={this.state.led}/>
                        </center><br />
                      <div className="widget-footer">
                        <button className="btn btn-sm btn-primary" type="button" onClick={ this.setLedColor }>
                          설정
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </JarvisWidget>
            </article>
            <article className="col-xs-12 col-sm-6">
              <JarvisWidget
                colorbutton={false}
                togglebutton={false}
                deletebutton={false}
                editbutton={false}
                fullscreenbutton={false}
                color="blue">
                <header>
                    <span className="widget-icon"> <i className="fa fa-bar-chart-o"/> </span>
                    <h2><strong>최근 온·습도</strong></h2>
                </header>
                <div className="no-padding">
                  <div>
                    <div className="col-xs-3 no-padding">
                      <div className="bg-color-redLight txt-color-white">
                        <div className="font-md text-center">
                          <i className="fa fa-arrow-up"/> {this.state.temperature_high} ℃
                        </div>
                        <div className="font-sm text-center">
                          <i>{this.convertDateFormat(this.state.temperature_high_dt)}</i>
                        </div>
                      </div>
                    </div>
                    <div className="col-xs-3 no-padding">
                      <div className="bg-color-blue txt-color-white">
                        <div className="font-md text-center">
                          <i className="fa fa-arrow-down"/> {this.state.temperature_low} ℃
                        </div>
                        <div className="font-sm text-center">
                          <i>{this.convertDateFormat(this.state.temperature_low_dt)}</i>
                        </div>
                      </div>
                    </div>
                    <div className="col-xs-3 no-padding">
                      <div className="bg-color-redLight txt-color-white text-center">
                        <div className="font-md">
                          <i className="fa fa-arrow-up"/> {this.state.humidity_high} %
                        </div>
                        <div className="font-sm text-center">
                          <i>{this.convertDateFormat(this.state.humidity_high_dt)}</i>
                        </div>
                      </div>
                    </div>
                    <div className="col-xs-3 no-padding">
                      <div className="bg-color-blue txt-color-white text-center">
                        <div className="font-md">
                          <i className="fa fa-arrow-down"/> {this.state.humidity_low} %
                        </div>
                        <div className="font-sm text-center">
                          <i>{this.convertDateFormat(this.state.humidity_low_dt)}</i>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row"></div>
                  <MorrisGraph data={this.state.sensorData}
                                type="line"
                                xkey={'crt_dt'}
                                ykeys={['temperature', 'humidity']}
                                labels={['온도', '습도']}
                                lineColors={['#a65858', '#57889c']}
                                resize="true"
                                hideHover="auto"
                                />
                </div>
              </JarvisWidget>
            </article>
            <article className="col-xs-12 col-sm-6">
              <JarvisWidget
                colorbutton={false}
                togglebutton={false}
                deletebutton={false}
                editbutton={false}
                fullscreenbutton={false}
                color="blue">
                <header>
                    <span className="widget-icon"> <i className="fa fa-bar-chart-o"/> </span>
                    <h2><strong>최근 조도</strong></h2>
                </header>
                <div className="no-padding">
                  <div>
                    <div className="col-xs-6 no-padding">
                      <div className="bg-color-redLight txt-color-white text-center">
                        <div className="font-md">
                          <i className="fa fa-arrow-up"/> {this.state.brightness_high} lux
                        </div>
                        <div className="font-sm text-center">
                          <i>{this.convertDateFormat(this.state.brightness_high_dt)}</i>
                        </div>
                      </div>
                    </div>
                    <div className="col-xs-6 no-padding">
                      <div className="bg-color-blue txt-color-white text-center">
                        <div className="font-md">
                          <i className="fa fa-arrow-down"/> {this.state.brightness_low} lux
                        </div>
                        <div className="font-sm text-center">
                          <i>{this.convertDateFormat(this.state.brightness_low_dt)}</i>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row"></div>
                  <MorrisGraph data={this.state.sensorData}
                                type="line"
                                xkey={'crt_dt'}
                                ykeys={['brightness']}
                                labels={['밝기(조도)']}
                                lineColors={['#b09b5b']}
                                resize="true"
                                hideHover="auto"
                                />
                </div>
              </JarvisWidget>
            </article>

          </div>

        </WidgetGrid>
      </div>
    )
  }
}


const SensorDataSource = {
  sensorData: [],
  lastSensor: {},
  recentSensor:{},
  total: 100,
  addSensorData: function(item) {
    if(this.lastSensor.crt_dt == null || this.lastSensor.crt_dt < item.crt_dt){
      this.lastSensor = item;
      if(this.sensorData.length >= this.total){
        this.sensorData.splice(0,1);
      }
      this.sensorData.push(item);
    }
  },
  getRecentData: function(){
    return this.recentSensor;
  },
  getRecentDataList: async function () {
    const size = this.sensorData.length == 0 ? 100 : 5
    const { data: { status, value } } = await Api.get('sensor?size='+size)
    if (status == 200 && value) {
      // 정렬
      value.collect.content.sort(function (a, b) {
                 return a.crt_dt - b.crt_dt;
             })
      value.collect.content = [].concat(value.collect.content)
      for(let i = 0; i < value.collect.content.length; i++){
        const item = value.collect.content[i];
        this.addSensorData(item);
      }
      this.recentSensor = value.recent;
      return this.sensorData;
    }
  }
};

const SensorLed = {
  setColor: async function (red, green, blue) {
    const { data: { status, value } } = await Api.get('sensor/led/'+red+'/'+green+'/'+blue)
    if (status == 200 && value) {
      return value;
    }
  }
}


const mapStateToProps = (state)=>(state.user)

export default connect(mapStateToProps)(Home)
