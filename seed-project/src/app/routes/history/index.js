export default {
  component: require('../../components/common/Layout').default,

  childRoutes: [
    {
      path: 'history',
      getComponent(nextState, cb){
        System.import('./containers/History').then((m)=> {
          cb(null, m.default)
        })
      }
    },
  ]

};
