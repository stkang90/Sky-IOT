import React from 'react'

import WidgetGrid from '../../../components/widgets/WidgetGrid'
import JarvisWidget  from '../../../components/widgets/JarvisWidget'

import Datatable from '../../../components/tables/Datatable'

export default class History extends React.Component {
  render() {
    return (
      <div id="content">
        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <h1 className="txt-color-blueDark">
              <i className="fa-fw fa fa-table"/> <b>
              센서기록</b></h1>
          </div>
        </div>
        <div className="row padding-10 padding-top-0">
          <Datatable
            options={{
              processing: true,
              serverSide: true,
              searching: false,
              ordering:  false,
              pageLength: 20,
              lengthMenu: [20, 50, 100],
              ajax: function(data, callback, settings) {
                var _data = {
                  size: data.length,
                  page: (data.start/data.length)
                };
                return $.ajax({
                  url: 'api/sensor/list',
                  data: _data,
                  dataType: "json",
                  success: function(data) {
                    callback({
                      recordsTotal: data.total_elements,
                      recordsFiltered:data.total_elements,
                      data : data.content
                    });
                  }
                });
              },
              columns: [
                {data: "id"},
                {data: "temperature"},
                {data: "humidity"},
                {data: "brightness"},
                {data: "heater", render: function ( data, type, row ) {
                  // If display or filter data is requested, format the date
                  if (type === 'display' || type === 'filter' ) {
                      return data ? "On" : "Off"
                  }
                  return data;
                }},
                {data: "fan", render: function ( data, type, row ) {
                  // If display or filter data is requested, format the date
                  if (type === 'display' || type === 'filter' ) {
                      return data ? "On" : "Off"
                  }
                  return data;
                }},
                {data: "crt_dt", render: function ( data, type, row ) {
                  // If display or filter data is requested, format the date
                  if ( type === 'display' || type === 'filter' ) {
                      var d = new Date( data );
                      return d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate()+' '+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
                  }
                  return data;
                }}]
            }}
             className="table table-striped table-bordered table-hover"
            width="100%">
            <thead>
            <tr>
              <th data-hide="phone,tablet">ID</th>
              <th data-class="expand"><i
                className="text-muted hidden-md hidden-sm hidden-xs"/>
                온도
              </th>
              <th data-hide="phone,tablet"><i
                className="text-muted hidden-md hidden-sm hidden-xs"/>
                습도
              </th>
              <th>조도</th>
              <th data-hide="phone,tablet"><i
                className="txt-color-blue hidden-md hidden-sm hidden-xs"/>
                온열기
              </th>
              <th data-hide="phone,tablet">선풍기</th>
              <th data-hide="expand"><i
                className="txt-color-blue hidden-md hidden-sm hidden-xs"/>
                시간
              </th>
            </tr>
            </thead>
          </Datatable>
        </div>
      </div>
    )
  }
}
