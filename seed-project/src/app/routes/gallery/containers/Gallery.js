import React from 'react'
import {connect} from 'react-redux';

import Api from '../../../components/api/api_client'
import SuperBoxGalery from '../../../components/ui/SuperBoxGallery'

import Upload from '../components/Upload'
const items = [
  {
    title:"aaa",
    image: "assets/img/superbox/superbox-full-1.jpg",
    description: "My first photoshop layer mask on a high end PSD template theme",
  },
];
class Gallery extends React.Component {
  state = {};
  getGalleryList = () => {
    GalleryDataSource.getDataList(this.props.category_id, 100).then((data)=>{
      this.setState({
          GalleryData: data
      });
    });
  };
  componentWillMount() {
    this.getGalleryList();
  }
  render() {
    return (
      <div id="content">
        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <h1 className="txt-color-blueDark">
              <i className="fa-fw fa fa-picture-o"/> <b>
              갤러리</b> <span className="font-xs">&gt; {this.props.title} </span>
            </h1>
          </div>
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-align-right">
            {(this.props.is_admin?
              <div>
                <button className="btn btn-default" data-toggle="modal" data-target="#myModal">
                      Upload
                </button>
              </div> : null
            )}
          </div>
        </div>
        <div className="row">
          <SuperBoxGalery className="superbox col-sm-12" items={this.state.GalleryData}/>
        </div>
        <Upload category_id={this.props.category_id}/>
      </div>
    )
  }
}
const GalleryDataSource = {
  IMAGE_BASE: "/upload/",
  getDataList: async function (category_id, size) {
    const { data: { status, value } } = await Api.get('gallery/'+category_id+'?size='+size)
    if (status == 200 && value) {
      var imgaeList = [];
      for(let i = 0; i < value.content.length; i++){
        const item = value.content[i];
        item.image = this.IMAGE_BASE + item.image;
        item.crt_dt =  new Date( item.crt_dt ).toISOString().substr(0,10);
      }
      this.recentSensor = value.recent;
      return value.content;
    }
  }
};


const mapStateToProps = (state)=>(state.user)

export default connect(mapStateToProps)(Gallery)
