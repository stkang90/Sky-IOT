export default {
  path: 'gallery',
  component: require('../../components/common/Layout').default,

  childRoutes: [
    {
      path: 'squirrel',
      getComponent(nextState, cb){
        System.import('./containers/squirrel').then((m)=> {
          cb(null, m.default)
        })
      }
    },
    {
      path: 'cage',
      getComponent(nextState, cb){
        System.import('./containers/cage').then((m)=> {
          cb(null, m.default)
        })
      }
    },
  ]

};
