import React from 'react'

import cookie from 'react-cookie'
import UiValidate from '../../../components/forms/validation/UiValidate'
import {Link} from 'react-router'
import {jQueryForm} from 'jquery-form'

const validationOptions = {
  // Rules for form validation
  rules: {
    title: {
      required: true
    },
    description: {
      required: true
    },
    img_file: {
      required: true
    }
  },

  // Messages for form validation
  messages: {
    title: {
      required: '제목을 입력해주세요.'
    },
    description: {
      required: '설명을 입력해주세요.'
    },
    img_file: {
      required: '이미지 파일을 선택해주세요.'
    }
  }
};

export default class Upload extends React.Component {
  state = {
    fileInputValue: ''
  };
  onSubmit = (e)=> {
    e.preventDefault();
    $('#upload-form').ajaxForm({
      url: '/api/gallery/upload/' + this.props.category_id,
      beforeSend: function(request) {
        request.setRequestHeader("Authorization", 'Bearer ' + cookie.load('access_token'));
      },
      complete: function() {
          window.location.reload(true);
      }
    });
    $("#upload-form").submit();
  }
  onFileInputChange = (e)=>{

    this.setState({
      fileInputValue: e.target.value
    })
  }
  render() {
  return (
    <div className="modal fade" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
          <div className="modal-dialog" style={ { width: '350px', margin: '10% auto' } }>
            <div className="modal-content">
              <UiValidate options={validationOptions}>
                <form id="upload-form" className="smart-form client-form" onSubmit={this.onSubmit} action="/api/gallery/upload/1" method="post" encType="multipart/form-data">
                  <header className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                    </button>
                    <h4 className="modal-title" id="myModalLabel">갤러리 업로드</h4>
                  </header>
                  <fieldset >
                    <section>
                      <label className="label">제목</label>
                      <label className="input">
                        <i className="icon-append fa fa-tag"/>
                        <input type="text" name="title" id="title"/>
                      </label>
                    </section>
                    <section>
                      <label className="label">설명</label>
                      <label className="textarea">
                        <i className="icon-append fa fa-comment"/>
                        <textarea rows="4" name="description" id="description"/>
                      </label>
                    </section>
                    <section>
                      <div className="input input-file">
                        <span className="button"><input id="img_file" type="file" name="img_file" onChange={this.onFileInputChange}/>찾기</span><input
                        type="text" value={this.state.fileInputValue} readOnly/>
                      </div>
                    </section>
                  </fieldset>
                  <footer className="modal-footer">
                    <button type="submit" className="btn btn-primary">
                      업로드
                    </button>
                  </footer>
                </form>
              </UiValidate>
            </div>
          </div>
        </div>
  )}
}
