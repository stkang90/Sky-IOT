import api from '~api'

const state = {
    lists: []
}

const actions = {
    async ['get']({commit}) {
        const { data: { status, value } } = await api.get('status')
        if (status === 200 && value) {
            commit('_get', value)
        }
    }
}

const mutations = {
    ['_get'](state, data) {
        data.content.sort(function (a, b) {
            return a.crt_dt - b.crt_dt
        })
        state.lists = [].concat(data.content)
    }
}

const getters = {
    ['get'](state) {
        console.debug("getters")
        return state.lists
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
