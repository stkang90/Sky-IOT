import api from '~api'
import toastr from 'toastr'
import {inBrowser} from '~utils'

toastr.options.positionClass = 'toast-top-center'

const state = {
    loading: false,
    progress: 0,
    showLoginModal: false,
    showRegisterModal: false,
    level: 0
}

const actions = {
    ['gProgress']({commit}, payload) {
        commit('progress', payload)
    },
    ['showMsg']({commit}, config) {
        let content, type
        if (typeof config === 'string') {
            content = config
            type = 'error'
        } else {
            content = config.content
            type = config.type
        }
        if (inBrowser) toastr[type](content)
    },
    ['hideMsg']() {
        toastr.clear()
    },
    async ['level']({ commit }) {
        const { data: { status, value} } = await api.get('user/level')
        if (status === 200 && value) {
            commit('_level', value.level)
        }
    }
}

const mutations = {
    ['progress'](state, payload) {
        state.progress = payload
    },
    ['showLoginModal'](state, payload) {
        state.showLoginModal = payload
    },
    ['showRegisterModal'](state, payload) {
        state.showRegisterModal = payload
    },
    ['_level'](state, payload) {
        state.level = payload
    }
}

const getters = {
    ['getGlobal'](state) {
        return state
    },
    ['isAdmin'](state) {
        return state.level > 2
    }
}

export default {
    actions,
    state,
    mutations,
    getters
}
