import api from '~api'

const state = {
    lists: {
        data: [],
        last: true,
        page: 0,
        path: ''
    },
    trending: []
}

const actions = {
    async ['get']({commit, rootState: {route: { path }}}, config) {
        const { data: { status, value } } = await api.get('posts', config)
        if (status === 200 && value) {
            commit('_get', {
                ...config,
                ...value,
                path
            })
        }
    },
    async ['getTrending']({ commit }) {
        const { data: { status, value } } = await api.get('posts/trending')
        if (status === 200 && value) {
            commit('_getTrending', value)
        }
    }
}

const mutations = {
    ['_get'](state, {content, last, page, path}) {
        if (page === 0) {
            content = [].concat(content)
        } else {
            content = state.lists.data.concat(content)
        }
        state.lists = {
            data: content, last, page, path
        }
    },
    ['_getTrending'](state, data) {
        state.trending = data.content
    },
    ['_insert'](state, payload) {
        if (state.lists.path) {
            state.lists.data = [payload].concat(state.lists.data)
        }
    },
    ['_update'](state, data) {
        const obj = state.lists.data.find(ii => ii.id === data.id)
        for (const jj in obj) {
            if (obj.hasOwnProperty(jj) && data[jj]) {
                obj[jj] = data[jj]
            }
        }
    },
    ['_delete'](state, id) {
        const obj = state.lists.data.find(ii => ii.id === id)
        if (obj) {
            const index = state.lists.data.indexOf(obj)
            state.lists.data.splice(index, 1)
        }
    },
}

const getters = {
    ['get'](state) {
        return state.lists
    },
    ['getTrending'](state) {
        return state.trending
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
