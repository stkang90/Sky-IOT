import api from '~api'

const state = {
    item: {
        data: {},
        path: '',
        isLoad: false
    }
}

const actions = {
    async ['get']({ commit, rootState: {route: { path, params: { id }}} }) {
        const { data: { status, value} } = await api.get('post/' + id, { markdown: 1})
        if (status === 200 && value) {
            commit('_get', {
                value,
                path
            })
        }
    }
}

const mutations = {
    ['_get'](state, {value, path}) {
        state.item = {
            value, path, isLoad: true
        }
    }
}

const getters = {
    ['get'](state) {
        return state.item
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
