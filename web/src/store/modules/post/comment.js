import api from '~api'

const state = {
    lists: {
        data: [],
        last: true,
        page: 0,
        path: ''
    }
}

const actions = {
    async ['getList']({commit, rootState: {route: { path, params: { id } }}}, config) {
        const { data: { status, value } } = await api.get('post/comment/' + id, { ...config })
        if (status === 200 && value) {
            commit('_getList', {
                ...config,
                ...value,
                path
            })
        }
    }
}

const mutations = {
    ['_getList'](state, {content, last, page, path}) {
        if (page === 1) {
            content = [].concat(content)
        } else {
            content = state.lists.data.concat(content)
        }
        state.lists = {
            data: content, last, page, path
        }
    },
    ['_insert'](state, data) {
        state.lists.data = [data].concat(state.lists.data)
    },
    ['_delete'](state, id) {
        const obj = state.lists.data.find(ii => ii._id === id)
        obj.is_delete = 1
    },
    ['_recover'](state, id) {
        const obj = state.lists.data.find(ii => ii._id === id)
        obj.is_delete = 0
    }
}

const getters = {
    ['getList'](state) {
        return state.lists
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
