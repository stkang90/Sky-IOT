import api from '~api'

const state = {
    lists: [],
    item: {}
}

const actions = {
    async ['getList']({ commit }, config) {
        const { data: { status, value } } = await api.get('post/category', config)
        if (status === 200 && value) {
            commit('_getList', value)
        }
    },
    async ['get'] ({commit, rootState: {route: { params: { id } }}}) {
        const { data: { status, value } } = await api.get('post/category/' + id)
        if (status === 200 && value) {
            commit('_get', value)
        }
    }
}

const mutations = {
    ['_getList'](state, payload) {
        state.lists = payload
    },
    ['_get'](state, payload) {
        state.item = payload
    },
    ['_insert'](state, payload) {
        state.lists = state.lists.concat([payload])
    },
    ['_update'](state, payload) {
        state.item = {
            ...state.item,
            ...payload
        }
        const obj = state.lists.find(ii => ii.id === payload.id)
        if (obj) {
            obj.name = payload.name
        }
    },
    ['_delete'](state, payload) {
        state.item = {
            ...state.item,
            ...payload
        }
        const obj = state.lists.find(ii => ii.id === payload)
        if (obj) {
            const index = state.lists.indexOf(obj)
            state.lists.splice(index, 1)
        }
    }
}

const getters = {
    ['getList'] (state) {
        return state.lists
    },
    ['get'] (state) {
        return state.item
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
