import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user/user'
import status from './modules/staus/status'
import post from './modules/post/post'
import postList from './modules/post/posts'
import postCategory from './modules/post/category'
import postComment from './modules/post/comment'
import global from './modules/global'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        status:{
            namespaced: true,
            ...status
        },
        post: {
            namespaced: true,
            ...post,
            modules: {
                posts: postList,
                category: postCategory,
                comment: postComment
            }
        },
        user: {
            namespaced: true,
            ...user
        },
        global: {
            namespaced: true,
            ...global
        }
    }
})
