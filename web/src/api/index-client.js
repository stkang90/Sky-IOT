import axios from 'axios'
import qs from 'qs'
import store from '../store'
import config from './config-client'
import Vue  from 'vue'
import VueCookie  from 'vue-cookie'

Vue.use(VueCookie)
axios.interceptors.request.use(config => {
    store.dispatch('global/gProgress', 50)
    return config
}, error => {
    return Promise.reject(error)
})

axios.interceptors.response.use(response => {
    store.dispatch('global/gProgress', 100)
    return response
}, error => {
    store.dispatch('global/gProgress', 100)
    // store.dispatch('global/showMsg', error.toString())
    return Promise.reject(error.response)
})

function _request(_method, _header, _url, _data){
    if(_method === 'get'){
        return axios({
            method: 'get',
            url: _url,
            _data,
            timeout: config.timeout,
            headers: _header
        })
    }
    _header['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'
    return axios({
        method: _method,
        url: _url,
        data: qs.stringify(_data),
        timeout: config.timeout,
        headers: _header
    })
}

function setResponseToken(response) {
    Vue.cookie.set('access_token', response.data.access_token, {expires: response.data.expires_in + 's'})
    Vue.cookie.set('refresh_token', response.data.refresh_token, {expires: '7D'})
    return response
}

async function requestRefreshToken(){
    const header = {
        "Authorization": "Basic " + btoa('sky-iot:')
    }
    const data = {
        grant_type: 'refresh_token',
        refresh_token: Vue.cookie.get('refresh_token')
    }
    const response = await _request('post', header, '/oauth/token', data)
    if(response.status !== 200){
        Vue.cookie.delete("access_token")
        Vue.cookie.delete("refresh_token")
        return false
    }
    setResponseToken(response)
    return true
}

const taskQueue = []
function checkTask(){
    return new Promise(function (resolve){
        taskQueue.push(resolve)
        if(taskQueue.length === 1){
            resolve(true)
        }
    })
}

function checkToken(){
    if(Vue.cookie.get('access_token')){
        return true
    }
    if(Vue.cookie.get('refresh_token')){
        return requestRefreshToken()
    }
    return false
}

async function _requestApi(method, url, data){
    await checkTask()
    const header = {}
    if(await checkToken()){
        header['Authorization'] = 'Bearer ' + Vue.cookie.get('access_token')
    }
    const response = await _request(method, header, config.api + url, data)
    // 토큰이 없거나, 잘못 됨
    if (response.status === 401) {
        // 토큰 삭제 후 재발급
        Vue.cookie.delete("access_token")
        if(await requestRefreshToken()){
            taskQueue.splice(0,1)
            if(taskQueue.length > 0){
                taskQueue[0](true)
            }
            return await _request(method, header, config.api + url, data)
        }
    }
    taskQueue.splice(0,1)
    if(taskQueue.length > 0){
        taskQueue[0](true)
    }
    return response
}
function regenerationData(response) {
    return {
        data: {
            status: response.status,
            value: response.data,
            message: response.statusText
        }
    }
}

function requestFail(response) {
    if (response.status === -500) {
        window.location.href = '/'
        return
    } else if (response.status === -400) {
        window.location.href = '/'
        return
    } else if (response.status !== 200) {
        store.dispatch('global/showMsg', response.data.error_description + '(Error : ' + response.status + ')')
    }
    return response
}

function requestApi(method, url, data) {
    return _requestApi(method, url, data).then(null, requestFail).then(regenerationData)
}

function requestLogin(data) {
    const header = {
        "Authorization": "Basic " + btoa('sky-iot:')
    }
    data['grant_type'] = 'password'
    const response = _request('post', header, '/oauth/token', data)
    return response.then(setResponseToken, requestFail).then(regenerationData)
}

export default {
    isLogin(){
        return Vue.cookie.get('access_token') || Vue.cookie.get('refresh_token')
    },
    login(data){
        return requestLogin(data)
    },
    get(url, params) {
        return requestApi('get', url, params)
    },
    post(url, data) {
        return requestApi('post', url, data)
    },
    patch(url, data) {
        return requestApi('patch', url, data)
    },
    delete(url, data) {
        return requestApi('delete', url, data)
    }

}
