import Vue from 'vue'
import VueRouter from 'vue-router'
import Meta from 'vue-meta'
import api from '~api'

import {inBrowser} from '../utils'

import index from '../pages/index.vue'
import about from '../pages/about.vue'

import post from '../pages/post/post.vue'
import posts from '../pages/post/posts.vue'

import postList from '../pages/post/list.vue'
import postInsert from '../pages/post/insert.vue'
import postModify from '../pages/post/modify.vue'
import postComment from '../pages/post/comment.vue'

import postCategoryList from '../pages/post/category-list.vue'
import postCategoryModify from '../pages/post/category-modify.vue'

import userAccount from '../pages/user/account.vue'
import userPassword from '../pages/user/password.vue'

Vue.use(VueRouter)
Vue.use(Meta)

const scrollBehavior = to => {
    const position = {}
    if (to.hash) {
        position.selector = to.hash
    }
    if (to.matched.some(mm => mm.meta.scrollToTop)) {
        position.x = 0
        position.y = 0
    }
    return position
}

const guardRoute = (to, from, next) => {
    var token = api.isLogin() || !inBrowser
    if (!token) {
        next('/')
    } else {
        next()
    }
}

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    scrollBehavior,
    routes: [
        { name:'index', path: '/', component: index },

        { name:'post', path: '/post/:id', component: post, meta: { scrollToTop: true } },

        { name:'posts', path: '/posts', component: posts },
        { name:'posts_category', path: '/posts/category/:category_id', component: posts },
        { name:'posts_search', path: '/posts/search/:key', component: posts },

        { name:'about', path: '/about', component: about, meta: { scrollToTop: true } },

        { name:'account', path: '/user/account', component: userAccount, meta: { scrollToTop: true }, beforeEnter: guardRoute },
        { name:'password', path: '/user/password', component: userPassword, meta: { scrollToTop: true }, beforeEnter: guardRoute },

        /*Admin*/
        { name:'post_category_list', path: '/admin/post/category/list', component: postCategoryList, meta: { scrollToTop: true }, beforeEnter: guardRoute },
        { name:'post_category_modify', path: '/admin/post/category/modify/:id', component: postCategoryModify, meta: { scrollToTop: true }, beforeEnter: guardRoute },
        { name:'post_list', path: '/admin/post/list', component: postList, meta: { scrollToTop: true }, beforeEnter: guardRoute },
        { name:'post_insert', path: '/admin/post/insert', component: postInsert, meta: { scrollToTop: true }, beforeEnter: guardRoute },
        { name:'post_modify', path: '/admin/post/:id', component: postModify, meta: { scrollToTop: true }, beforeEnter: guardRoute },
        { name:'post_comment', path: '/admin/post/comment/:id', component: postComment, meta: { scrollToTop: true }, beforeEnter: guardRoute }
    ]
})



export default router
